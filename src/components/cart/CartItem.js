import { useContext, useEffect, useState } from "react";
import { Card, Col, Container, Image, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { getSingleProduct } from "../../api/ProductsApi";
import { CheckOutContext } from "../store/CheckOutContext";
import "./CartItem.css";
import CartItemVariation from "./CartItemVariation";

export default function CartItem(props) {
    const checkOutContext = useContext(CheckOutContext);
    const [hasDiscount, setHasDiscount] = useState(false);
    const [isChecked, setIsChecked] = useState(false);
    const [loadedProductImages, setLoadedProductImages] = useState([]);
    const [isItemLoading, setIsItemLoading] = useState(true);
    const { productId, quantity } = useParams();

    useEffect(() => {
        getProductImages();
    }, [isItemLoading]);

    const getProductImages = async () => {
        const productDetails = await getSingleProduct(props.productId);
        setLoadedProductImages(productDetails.productImages);
        setIsItemLoading(false);
    };

    useEffect(() => {
        if (props.discount !== "0%") {
            setHasDiscount(true);
        }
    }, [props.discount]);

    const onTickHandler = () => {
        setIsChecked(!isChecked);
        isChecked ? checkOutContext.onUncheck(props) : checkOutContext.onCheck(props);
    };

    useEffect(() => {
        if (productId && quantity && productId === props.productId) {
            onTickHandler();
        }
    }, []);
    return isItemLoading ? (
        <h4>Loading...</h4>
    ) : (
        <Container>
            <Card className="cartCard">
                <li>
                    <Row>
                        <Col className="col-8">
                            <Container fluid>
                                <Row>
                                    <Col className="col-1 d-flex justify-content-center text-algin-center">
                                        <input
                                            type="checkbox"
                                            id={props.productId}
                                            name={props.productName}
                                            value={props.productId}
                                            checked={isChecked}
                                            onChange={onTickHandler}
                                        />
                                    </Col>

                                    <Col className="col-3 ">
                                        {loadedProductImages && (
                                            <Image
                                                fluid
                                                src={`https://res.cloudinary.com/dpxtfxw52/image/upload/w_1000,h_1000,c_fill,q_100/${loadedProductImages[0]}`}
                                                alt={props.productName}
                                            />
                                        )}
                                    </Col>
                                    <Col className="col-5 productNameCartItem py-5">
                                        <Container>
                                            <Row>
                                                <Col className="col-12 mx-0">
                                                    {props.productName}
                                                </Col>
                                                {props.variations && (
                                                    <Col className="col-12 mx-0 p-2">
                                                        {props.variations.map((variation) => {
                                                            return (
                                                                <CartItemVariation
                                                                    variation={variation.variation}
                                                                />
                                                            );
                                                        })}
                                                    </Col>
                                                )}
                                            </Row>
                                        </Container>
                                    </Col>
                                    <Col className="col-3 d-flex aling-items-center justify-content-center">
                                        <button
                                            type="button"
                                            className="minusBtnCart"
                                            onClick={props.onRemove}
                                        >
                                            -
                                        </button>
                                        <Card.Text className="quantityInCart">
                                            x {props.quantity}
                                        </Card.Text>
                                        <button
                                            type="button"
                                            className="addBtnInCart"
                                            onClick={props.onAdd}
                                        >
                                            +
                                        </button>
                                    </Col>
                                </Row>
                            </Container>
                        </Col>

                        <Col className="col-4">
                            <Card className="border-0">
                                <Card.Text> ₱ {props.originalPrice}</Card.Text>
                                {hasDiscount && <Card.Text>Discount: {props.discount}</Card.Text>}
                                {hasDiscount && (
                                    <Card.Text>Discounted Price: ₱{props.totalPrice}</Card.Text>
                                )}
                                <Card.Text>
                                    Total Amount: ₱
                                    {(hasDiscount
                                        ? props.totalPrice * props.quantity
                                        : props.originalPrice * props.quantity
                                    ).toFixed(2)}
                                </Card.Text>
                            </Card>
                        </Col>
                    </Row>
                </li>
            </Card>
        </Container>
    );
}
