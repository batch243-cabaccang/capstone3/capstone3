import { Fragment, useContext, useEffect, useState } from "react";
import { checkOutItems } from "../../api/CheckOutApi";
import { CartContext } from "../store/CartContext";
import { CheckOutContext } from "../store/CheckOutContext";
import CartItem from "./CartItem";
import CheckOut from "../cart/CheckOut";
import { Button, Card, Col, Container, ListGroup, Row, CloseButton } from "react-bootstrap";
import "./UserCart.css";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { OrderContext } from "../store/OrderContext";

export default function UserCart() {
    const cartContext = useContext(CartContext);
    const checkOutContext = useContext(CheckOutContext);
    const orderContext = useContext(OrderContext);
    const [isCartLoading, setIsCartLoading] = useState(true);
    const [isDisabled, setIsDisabled] = useState(false);
    const [isCheckingOut, setIsCheckingOut] = useState(false);
    const [chosenPaymentMethod, setChosenPaymentMethod] = useState("Cash On Delivery");
    const [orderPlaced, setOrderPlaced] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        checkOutContext.toCheckOutItems.length === 0 ? setIsDisabled(true) : setIsDisabled(false);
    }, [checkOutContext.toCheckOutItems.length]);

    useEffect(() => {
        reLoadUserCart();
    }, [isCheckingOut, orderPlaced]);

    const reLoadUserCart = async () => {
        cartContext.loadUserCart();
        setIsCartLoading(false);
    };

    const onAdd = (item) => {
        cartContext.addProduct({ ...item, quantity: 1 });
    };
    const onRemove = (item) => {
        cartContext.removeProduct(item);
    };

    const checkOutHandler = async () => {
        const response = await checkOutItems(checkOutContext, chosenPaymentMethod);
        orderContext.onGetAll();
        if (response === "Transaction Successful!") {
            Swal.fire({
                title: response,
                icon: "success",
                text: "Your Order Has Been Placed!",
            }).then(() => {
                navigate("/shop");
            });
            setOrderPlaced(true);
        } else {
            Swal.fire({
                title: response,
                icon: "error",
                text: "Something went wrong!",
            });
        }
    };

    if (isCartLoading) {
        return (
            <div>
                <div>User Cart</div>
                <h4>Cart Is Loading...</h4>
            </div>
        );
    }

    console.log(cartContext.items);

    return isCartLoading ? (
        <h4>Cart Loading...</h4>
    ) : (
        <Fragment>
            <Card>
                {isCheckingOut && (
                    <CheckOut
                        isCheckingOut={isCheckingOut}
                        setIsCheckingOut={setIsCheckingOut}
                        checkOutHandler={checkOutHandler}
                        setChosenPaymentMethod={setChosenPaymentMethod}
                    />
                )}
                <div>User Cart</div>
                {cartContext.items.length === 0 && <h4>Cart Is Empty</h4>}
                {cartContext.items.length >= 1 && (
                    <ListGroup className="cartItems d-flex align-items-center border-0">
                        {cartContext.items.map((item) => {
                            return (
                                <ListGroup.Item className="w-100 border-0" key={item._id}>
                                    <CartItem
                                        key={item._id}
                                        _id={item._id}
                                        item={item}
                                        variations={item.variations}
                                        productImage={item.productImage}
                                        productId={item.productId}
                                        productName={item.productName}
                                        originalPrice={item.originalPrice}
                                        discount={item.discount}
                                        totalPrice={item.totalPrice}
                                        quantity={item.quantity}
                                        onAdd={onAdd.bind(null, item)}
                                        onRemove={onRemove.bind(null, item)}
                                    />
                                </ListGroup.Item>
                            );
                        })}
                    </ListGroup>
                )}
                <Container className="d-flex justify-content-end border-0">
                    <Row>
                        <Col className="col-12">
                            <Card.Text>
                                Total Items: {checkOutContext.totalItemsForCheckout}
                            </Card.Text>
                        </Col>
                        <Col className="col-12">
                            <Card.Text>
                                Total Amount: ₱
                                {Math.abs(checkOutContext.totalAmountToPay.toFixed(2))}
                            </Card.Text>
                        </Col>
                        <Button
                            className="bg-gradient"
                            type="button"
                            disabled={isDisabled}
                            onClick={() => {
                                setIsCheckingOut(true);
                            }}
                        >
                            Check out
                        </Button>
                    </Row>
                </Container>
            </Card>
        </Fragment>
    );
}
