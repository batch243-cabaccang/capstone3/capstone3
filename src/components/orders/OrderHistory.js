import { Fragment, useContext, useEffect, useMemo, useState } from "react";
import ListGroup from "react-bootstrap/ListGroup";
import { retrieveOrders } from "../../api/OrdersApi";
import SearchBar from "../SearchBar";
import { OrderContext } from "../store/OrderContext";
import Orders from "./Orders";

export default function OrderHistory() {
    const orderContext = useContext(OrderContext);
    const [loadedOrders, setLoadedOrders] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    // useEffect(() => {
    //     setIsLoading(!isLoading);
    // }, [orderContext.orders]);

    return (
        <Fragment>
            <SearchBar />

            {!orderContext.orders ? (
                <h4>Loading...</h4>
            ) : (
                <ListGroup>
                    {orderContext.orders.map((order) => {
                        return (
                            <ListGroup.Item key={order._id}>
                                <Orders orderDetails={order} />
                            </ListGroup.Item>
                        );
                    })}
                </ListGroup>
            )}
        </Fragment>
    );
}
