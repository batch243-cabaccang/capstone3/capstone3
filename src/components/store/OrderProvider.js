import { useEffect, useState } from "react";
import { getFilteredOrder, retrieveOrders } from "../../api/OrdersApi";
import { OrderContext } from "./OrderContext";

export default function OrderProvider(props) {
    const [loadedOrders, setLoadedOrders] = useState();

    useEffect(() => {
        getAllUserOrders();
    }, []);

    const getAllUserOrders = async () => {
        const allOrders = await retrieveOrders();
        setLoadedOrders(allOrders);
    };

    const searchHandler = async (searchOrdersBy) => {
        const orders = await getFilteredOrder(searchOrdersBy);
        setLoadedOrders(orders);
    };

    const orderContext = {
        orders: loadedOrders,
        onSearch: searchHandler,
        onGetAll: getAllUserOrders,
    };

    return <OrderContext.Provider value={orderContext}>{props.children}</OrderContext.Provider>;
}
