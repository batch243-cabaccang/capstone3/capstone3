import { createContext } from "react";

export const CartContext = createContext({
    items: [],
    totalItems: 0,
    totalAmount: 0,
    addProduct: (item) => {},
    removeProduct: (item) => {},
    loadUserCart: () => {},
});
