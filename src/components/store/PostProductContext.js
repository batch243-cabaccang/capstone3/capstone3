import { createContext } from "react";

export const PostProductContext = createContext({
    productVariations: [],
    productImagesPreview: [],
    addImages: (images) => {},
    addVariation: (variation) => {},
    removeVariation: (variation) => {},
});
