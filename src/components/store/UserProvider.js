import { useState } from "react";
import { UserContext } from "./UserContext";

export function UserProvider(props) {
    const isAdmin = localStorage.getItem("isAdmin");
    const token = localStorage.getItem("TestToken");
    const [userToken, setUserToken] = useState(token);
    const [userAdmin, setUserAdmin] = useState(isAdmin);

    const userContext = {
        isAdmin: userAdmin,
        token: userToken,
        setUserToken: setUserToken,
        setUserAdmin: setUserAdmin,
    };

    return <UserContext.Provider value={userContext}>{props.children}</UserContext.Provider>;
}
