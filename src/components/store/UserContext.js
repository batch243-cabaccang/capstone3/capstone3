import { createContext } from "react";

export const UserContext = createContext({
    isAdmin: "",
    token: "",
    setUserToken: (token) => {},
    setUserAdmin: (isAdmin) => {},
});
