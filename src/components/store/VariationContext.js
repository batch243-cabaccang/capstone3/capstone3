import { createContext } from "react";

export const VariationContext = createContext({
    variations: [],
    addVariation: (variations) => {},
    removeVariation: (variations) => {},
});
