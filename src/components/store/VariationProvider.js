import { useReducer } from "react";
import { VariationContext } from "./VariationContext";

const ADD_VARIATIONS = "addVariations";

const defaultVariations = {
    variations: [],
};

const variationsReducer = (state, action) => {
    if (action.actionType === ADD_VARIATIONS) {
        console.log(action.variation);
        const updatedVariations = [...state.variations, action.variation];

        return {
            variations: updatedVariations,
        };
    }
    return {
        defaultVariations,
    };
};

export function VariationProvider(props) {
    const [variationsState, dispatchAction] = useReducer(variationsReducer, defaultVariations);

    const onAddVariation = (variation) => {
        console.log(variation);
        dispatchAction({ actionType: ADD_VARIATIONS, variation: variation });
    };

    const onRemoveVariation = () => {};

    console.log(variationsState.variations);

    const variationContext = {
        variations: variationsState.variations,
        addVariation: onAddVariation,
        removeVariation: onRemoveVariation,
    };

    return (
        <VariationContext.Provider value={variationContext}>
            {props.children}
        </VariationContext.Provider>
    );
}
