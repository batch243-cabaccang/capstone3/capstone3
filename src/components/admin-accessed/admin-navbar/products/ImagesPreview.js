import { Fragment } from "react";
import Image from "react-bootstrap/Image";

export default function ImagesPreview(props) {
    return (
        <Fragment>
            <Image fluid src={URL.createObjectURL(props.image)} />
        </Fragment>
    );
}
