import { Fragment } from "react";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function AdminPage() {
    const navigate = useNavigate();
    return (
        <Fragment>
            <Button
                onClick={() => {
                    navigate("/postProduct");
                }}
            >
                Post Product
            </Button>
            <Button
                onClick={() => {
                    navigate("/shop");
                }}
            >
                Show All Products
            </Button>
            <Button
                onClick={() => {
                    navigate("/allOrderHistory");
                }}
            >
                User Orders
            </Button>
        </Fragment>
    );
}
