import { useContext, useEffect, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { userLogin, userRegistration } from "../api/UserApi";
import { Form, Button, Card, Container, Row, Col } from "react-bootstrap";
import { UserContext } from "./store/UserContext";
import "./Registration.css";
import Swal from "sweetalert2";
import { CartContext } from "./store/CartContext";
import { OrderContext } from "./store/OrderContext";

export default function Registration() {
    const userContext = useContext(UserContext);
    const cartContext = useContext(CartContext);
    const orderContext = useContext(OrderContext);

    const [passwordMatch, setPasswordMatch] = useState(true);
    const [conPasswordInput, setConPassword] = useState("");
    const [isFormValid, setIsFormValid] = useState(false);

    const [emailInput, seteEmailInput] = useState("");
    const [passwordInput, setPasswordInput] = useState("");
    const [firstNameInput, setFirstNameInput] = useState("");
    const [lastNameInput, setLastNameInput] = useState("");
    const [mobileNumInput, setMobileNumInput] = useState("");
    const [streetNumberAndNameInput, setStreetNumberAndNameInput] = useState("");
    const [brgyDistrictInput, setBrgyDistrictInput] = useState("");
    const [cityMunicipalityInput, setCityMunicipalityInput] = useState("");
    const [countryInput, setCountryInput] = useState("");
    const [postalCodeInput, setPostalCodeInput] = useState("");
    const navigate = useNavigate();

    const registrationHandler = async (event) => {
        event.preventDefault();

        const userDetails = {
            email: emailInput,
            password: passwordInput,
            firstName: firstNameInput,
            lastName: lastNameInput,
            mobileNum: mobileNumInput,
            streetNumberAndName: streetNumberAndNameInput,
            brgyDistrict: brgyDistrictInput,
            cityMunicipality: cityMunicipalityInput,
            country: countryInput,
            postalCode: postalCodeInput,
        };

        const newUser = await userRegistration(userDetails);
        if (newUser.message === "Email Is Taken") {
            Swal.fire({
                title: newUser.message,
                icon: "error",
                text: "Please Use a Different Email Address.",
            });
            return;
        }
        if (newUser.message === "User Created") {
            const userCredentials = { email: emailInput, password: passwordInput };
            const user = await userLogin(userCredentials);
            if (!user.token) {
                return alert(user);
            }
            if (user.isAdmin) {
                localStorage.setItem("isAdmin", user.isAdmin);
            }

            localStorage.setItem("TestToken", user.token);
            userContext.setUserToken(localStorage.getItem("TestToken"));
            userContext.setUserAdmin(localStorage.getItem("isAdmin"));
            cartContext.loadUserCart();
            orderContext.onGetAll();

            Swal.fire({
                title: "You are now Registered!",
                icon: "success",
                text: "We'll Log You Right in.",
            });
            navigate("/shop", { replace: true });
            return;
        }
    };

    useEffect(() => {
        if (
            emailInput &&
            passwordMatch &&
            firstNameInput &&
            lastNameInput &&
            mobileNumInput &&
            streetNumberAndNameInput &&
            brgyDistrictInput &&
            cityMunicipalityInput &&
            countryInput &&
            postalCodeInput
        )
            return setIsFormValid(true);
        setIsFormValid(false);
    }, [
        emailInput,
        passwordMatch,
        firstNameInput,
        lastNameInput,
        mobileNumInput,
        streetNumberAndNameInput,
        brgyDistrictInput,
        cityMunicipalityInput,
        countryInput,
        postalCodeInput,
    ]);

    useEffect(() => {
        if (conPasswordInput === passwordInput) {
            return setPasswordMatch(true);
        }
        setPasswordMatch(false);
    }, [passwordInput, conPasswordInput]);

    return userContext.token ? (
        <Navigate to="/shop" />
    ) : (
        <Container fluid className="d-flex justify-content-center text-align-center my-3">
            <Card className="registrationCard">
                <Form onSubmit={registrationHandler}>
                    <Form.Label className="d-flex justify-content-center text-align-center m-4 registrationHeader">
                        Registration
                    </Form.Label>
                    <Row className="m-3">
                        <Col className="col-6">
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter email"
                                    value={emailInput}
                                    onChange={(event) => {
                                        seteEmailInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    value={passwordInput}
                                    onChange={(event) => {
                                        setPasswordInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="confirmPassword">
                                <Form.Label>Confirm Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Confirm Password"
                                    value={conPasswordInput}
                                    onChange={(event) => {
                                        setConPassword(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="firstName">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="First Name"
                                    value={firstNameInput}
                                    onChange={(event) => {
                                        setFirstNameInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="lastName">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Last Name"
                                    value={lastNameInput}
                                    onChange={(event) => {
                                        setLastNameInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="mobileNumber">
                                <Form.Label>Mobile Number</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Mobile Number"
                                    value={mobileNumInput}
                                    onChange={(event) => {
                                        setMobileNumInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-3" controlId="street">
                                <Form.Label>Street</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Street"
                                    value={streetNumberAndNameInput}
                                    onChange={(event) => {
                                        setStreetNumberAndNameInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="baranggay">
                                <Form.Label>Baranggay</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Baranggay"
                                    value={brgyDistrictInput}
                                    onChange={(event) => {
                                        setBrgyDistrictInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="city">
                                <Form.Label>City</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="City"
                                    value={cityMunicipalityInput}
                                    onChange={(event) => {
                                        setCityMunicipalityInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="country">
                                <Form.Label>Country</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Country"
                                    value={countryInput}
                                    onChange={(event) => {
                                        setCountryInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="zipCode">
                                <Form.Label>Zip Code</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Zip Code"
                                    value={postalCodeInput}
                                    onChange={(event) => {
                                        setPostalCodeInput(event.target.value);
                                    }}
                                />
                            </Form.Group>

                            <Button
                                className="mt-3 w-100 submitBtn"
                                variant="primary"
                                type="submit"
                                disabled={!isFormValid}
                            >
                                Register
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Card>
        </Container>
    );
}
