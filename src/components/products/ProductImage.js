import { Col, Image } from "react-bootstrap";

export default function ProductImage(props) {
    return (
        <Image
            fluid
            src={`https://res.cloudinary.com/dpxtfxw52/image/upload/w_1000,h_1000,c_fill,q_100/${props.productImage}`}
            alt="hehe"
            onClick={() => {
                props.setImageClicked(
                    `https://res.cloudinary.com/dpxtfxw52/image/upload/w_1000,h_1000,c_fill,q_100/${props.productImage}`
                );
            }}
        />
    );
}
