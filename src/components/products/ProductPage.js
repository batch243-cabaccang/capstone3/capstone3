import "./ProductPage.css";
import { useContext, useEffect, useRef, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { getSingleProduct } from "../../api/ProductsApi";
import { Container, Row, Col, Image, Card, CloseButton } from "react-bootstrap";

import { CartContext } from "../store/CartContext";
import ProductVariationList from "./ProductVariationList";
import ProductImage from "./ProductImage";
import { VariationContext } from "../store/VariationContext";

export default function ProductPage() {
    const cartContext = useContext(CartContext);
    const variationContext = useContext(VariationContext);
    const [quantityInput, setQuantityInput] = useState(1);
    const [loadedDetails, setLoadedDetails] = useState([]);
    const [imageClicked, setImageClicked] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const { productId } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        getProductDetails();
    }, []);

    const getProductDetails = async () => {
        const productDetails = await getSingleProduct(productId);
        setLoadedDetails(productDetails);
        setIsLoading(false);
    };

    const onAddHandler = (event) => {
        event.preventDefault();

        const enteredQuantity = quantityInput;
        const enteredQuantityAsNumber = +enteredQuantity;

        const productDetails = {
            productId: loadedDetails._id,
            productImages: loadedDetails.productImages,
            productName: loadedDetails.productName,
            variations: variationContext.variations,
            originalPrice: loadedDetails.originalPrice,
            totalPrice: loadedDetails.totalPrice,
            discount: loadedDetails.discount,
            description: loadedDetails.description,
            stock: loadedDetails.stock,
            productType: loadedDetails.productType,
        };

        cartContext.addProduct({
            productDetails: productDetails,
            quantity: enteredQuantityAsNumber,
        });
    };

    return isLoading ? (
        <h4>Loading...</h4>
    ) : (
        <Container className="d-flex text-align-center">
            <Card className="productPageCard p-3 mt-2">
                <CloseButton
                    type="button"
                    onClick={() => {
                        navigate("/shop");
                    }}
                />
                <Row xs={1} md={2} className="m-1 mt-3">
                    <Col className="" lg={4}>
                        <Row className="imageDisplayer">
                            <Col lg={12}>
                                <Image
                                    fluid
                                    src={
                                        imageClicked
                                            ? imageClicked
                                            : `https://res.cloudinary.com/dpxtfxw52/image/upload/w_1000,h_1000,c_fill,q_100/${loadedDetails.productImages[0]}`
                                    }
                                    alt={loadedDetails.productName}
                                />
                            </Col>
                        </Row>
                        <Row className="imageSelector d-flex align-items-center">
                            {loadedDetails.productImages.map((productImage) => {
                                return (
                                    <Col
                                        id="imageSelector-col mx-1 p-0"
                                        lg={3}
                                        key={Math.floor(Math.random() * new Date())}
                                    >
                                        <ProductImage
                                            key={Math.floor(Math.random() * new Date())}
                                            productImage={productImage}
                                            className="imageSelectorScroll"
                                            setImageClicked={setImageClicked}
                                        />
                                    </Col>
                                );
                            })}
                        </Row>
                        <Row className="imageFooter">
                            <Col lg={12}>extras here</Col>
                        </Row>
                    </Col>

                    <Col className="ps-5" lg={8}>
                        <form onSubmit={onAddHandler}>
                            <Row className="">
                                <Col className=" m-1">
                                    <Row>
                                        <Col className="col-12 my-1">
                                            <h4>{loadedDetails.productName}</h4>
                                        </Col>
                                        <Col className="col-12 my-1">
                                            <Row>
                                                <Col className="col-2  d-flex justify-content-center align-items-center border-end">
                                                    <h4 className="ratingsAndStuffNumbers">4.9</h4>
                                                    <h6 className="ratingsAndStuff">
                                                        &nbsp;"stars"
                                                    </h6>
                                                </Col>
                                                <Col className="col-2 d-flex justify-content-center align-items-center border-end">
                                                    <h4 className="ratingsAndStuffNumbers">1.2k</h4>
                                                    <h6 className="ratingsAndStuff">
                                                        &nbsp;Ratings
                                                    </h6>
                                                </Col>
                                                <Col className="col-2  d-flex justify-content-center align-items-center">
                                                    <h4 className="ratingsAndStuffNumbers">
                                                        {loadedDetails.totalBuyCount}
                                                    </h4>
                                                    <h6 className="ratingsAndStuff">&nbsp;Sold</h6>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row className="pricePayable d-flex align-items-center">
                                <Col className={loadedDetails.hasDiscount ? "col-2" : "col-3"}>
                                    <h6
                                        className={
                                            loadedDetails.hasDiscount ? "priceSlash" : "totalPrice"
                                        }
                                    >
                                        ₱{loadedDetails.originalPrice}
                                    </h6>
                                </Col>

                                {loadedDetails.hasDiscount && (
                                    <Col className="col-3">
                                        <h6 className="totalPrice">₱{loadedDetails.totalPrice}</h6>
                                    </Col>
                                )}
                                {loadedDetails.hasDiscount && (
                                    <Col className="col-2 d-flex text-align-center p-0">
                                        <h6 className="discountDisplay">
                                            {loadedDetails.discount} Off
                                        </h6>
                                    </Col>
                                )}
                            </Row>
                            {loadedDetails.variations.length !== 0 && (
                                <Row>
                                    <Col>
                                        <ProductVariationList
                                            productVariations={loadedDetails.variations}
                                        />
                                    </Col>
                                </Row>
                            )}
                            <Row className="quantity">
                                <Col>
                                    <Row>
                                        <Col className="col-12 my-3">
                                            <div className="quantityLabel">
                                                <span>Quantity</span>
                                            </div>

                                            <button
                                                type="button"
                                                className="minusBtn"
                                                onClick={() => {
                                                    if (quantityInput !== 1)
                                                        return setQuantityInput(quantityInput - 1);
                                                }}
                                            >
                                                -
                                            </button>
                                            <input
                                                className="quantityInput"
                                                type="number"
                                                min="1"
                                                max="5"
                                                value={quantityInput}
                                                onChange={(event) => {
                                                    setQuantityInput(+event.target.value);
                                                }}
                                            />
                                            <button
                                                type="button"
                                                className="addBtn"
                                                onClick={() => {
                                                    setQuantityInput(quantityInput + 1);
                                                }}
                                            >
                                                +
                                            </button>
                                            <h6 className="ratingsAndStuff ms-4">
                                                {loadedDetails.stock}&nbsp;in stock
                                            </h6>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <Col className="col-4 addToCartCol">
                                                    <button className="addToCart" type="submit">
                                                        <img
                                                            src={require("../../assets/icons8-shopping-cart-64.png")}
                                                            className="addToCartIcon"
                                                        />
                                                        Add To Cart
                                                    </button>
                                                </Col>
                                                <Col className="p-0">
                                                    <button
                                                        className="buyNow"
                                                        onClick={async (event) => {
                                                            onAddHandler(event);
                                                            setTimeout(() => {
                                                                navigate(
                                                                    `/userCart/${loadedDetails._id}/${quantityInput}`
                                                                );
                                                            }, 50);
                                                        }}
                                                    >
                                                        Buy Now
                                                    </button>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
            </Card>
        </Container>
    );
}
