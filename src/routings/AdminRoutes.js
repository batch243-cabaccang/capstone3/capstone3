import { Navigate, Outlet } from "react-router-dom";

export default function AdminRoutes() {
    const isAdmin = localStorage.getItem("isAdmin");
    return isAdmin ? <Outlet /> : <Navigate to="/pageRestricted" />;
}
