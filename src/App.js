import "./App.css";
import { Fragment, useContext } from "react";
import { Route, Routes } from "react-router-dom";
import PostProduct from "./components/admin-accessed/admin-navbar/products/PostProduct";
import UsersList from "./components/admin-accessed/UsersList";
import UserCart from "./components/cart/UserCart";
import Home from "./components/Home";
import Login from "./components/Login";
import Registration from "./components/Registration";
import MainNavbar from "./navigation-bar/MainNavbar";
import CartProvider from "./components/store/CartProvider";
import CheckOutProvider from "./components/store/CheckOutProvider";
import CheckOut from "./components/cart/CheckOut";
import ProductPage from "./components/products/ProductPage";
import { UserProvider } from "./components/store/UserProvider";
import { UserContext } from "./components/store/UserContext";
import AdminRoutes from "./routings/AdminRoutes";
import PageRestricted from "./components/pages/PageRestricted";
import LoggedInRoutes from "./routings/LoggedInRoutes";
import PostProductProvider from "./components/store/PostProductProvider";
import UpdateProduct from "./components/products/UpdateProduct";
import AdminPage from "./components/admin-accessed/admin-navbar/products/AdminPage";
import ShowProducts from "./components/ShowProducts";
import OrderHistory from "./components/orders/OrderHistory";
import OrderProvider from "./components/store/OrderProvider";
import AdminAllOrderHistory from "./components/admin-accessed/admin-navbar/products/AdminAllOrderHistory";
import { VariationProvider } from "./components/store/VariationProvider";

function App() {
    const userContext = useContext(UserContext);
    // const [cartUpdated, setCartUpdated] = useState([]);
    // const [checkOutUpdated, setCheckOutUpdated] = useState([]);

    // const cartProvider = useMemo(
    //     () => ({ cartUpdated, setCartUpdated }),
    //     [cartUpdated, setCartUpdated]
    // );

    // const checkOutProvider = useMemo(
    //     () => ({ checkOutUpdated, setCheckOutUpdated }),
    //     [checkOutUpdated, setCheckOutUpdated]
    // );

    return (
        <UserProvider>
            <OrderProvider>
                <VariationProvider>
                    <PostProductProvider>
                        <CartProvider>
                            <CheckOutProvider>
                                <MainNavbar />
                                <Routes>
                                    <Route path="*" exact element={<ShowProducts />} />
                                    {/* <Route path="/home" exact element={<Home />} /> */}

                                    <Route path="/login" exact element={<Login />} />

                                    <Route path="/registration" exact element={<Registration />} />

                                    <Route path="/shop" exact element={<ShowProducts />} />
                                    <Route path="/shop/:filter" element={<ShowProducts />} />

                                    <Route element={<LoggedInRoutes />}>
                                        <Route path="/orders" element={<OrderHistory />} />
                                    </Route>

                                    <Route
                                        path="/product/:productName/:productId"
                                        exact
                                        element={<ProductPage />}
                                    />
                                    <Route path="/postProduct" exact element={<PostProduct />} />

                                    <Route element={<AdminRoutes />}>
                                        <Route path="/userList" element={<UsersList />} />
                                        <Route
                                            path="/product/:productName/:productId/update"
                                            element={<UpdateProduct />}
                                        />
                                        <Route path="/adminPage" element={<AdminPage />} />
                                        <Route
                                            path="/allOrderHistory"
                                            element={<AdminAllOrderHistory />}
                                        />
                                    </Route>

                                    <Route path="/userCart" exact element={<UserCart />} />
                                    <Route
                                        path="/userCart/:productId/:quantity"
                                        exact
                                        element={<UserCart />}
                                    />
                                    <Route path="/checkout" exact element={<CheckOut />} />
                                    <Route
                                        path="/pageRestricted"
                                        exact
                                        element={<PageRestricted />}
                                    />
                                </Routes>
                            </CheckOutProvider>
                        </CartProvider>
                    </PostProductProvider>
                </VariationProvider>
            </OrderProvider>
        </UserProvider>
    );
}

export default App;
